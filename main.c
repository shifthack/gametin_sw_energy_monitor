#include <stdio.h>
#include "em_device.h"
#include "em_chip.h"
#include "bsp_trace.h"

#include "em_emu.h"
#include "em_cmu.h"
#include "em_adc.h"
#include "rtcdrv.h"

#define ITM_Port32(n) (*((volatile unsigned int *)(0xE0000000+4*n)))

/* Need to implement the  two Retarget IO functions with the read/write functions we want to use. */
/* This retargets printf to the SWO output. */
int RETARGET_WriteChar(char c){
  return ITM_SendChar (c);
}

int RETARGET_ReadChar(void){
  return 0;
}

/**************************************************************************//**
 * @brief Configure SWO - serial wire output
 *****************************************************************************/
void SWO_Setup(void)
{
  uint32_t *dwt_ctrl = (uint32_t *) 0xE0001000;
  uint32_t *tpiu_prescaler = (uint32_t *) 0xE0040010;
  uint32_t *tpiu_protocol = (uint32_t *) 0xE00400F0;
  uint32_t *tpiu_ffcr = (uint32_t *) 0xE0040304;

  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;

  /* Enable Serial wire output pin */
  GPIO->ROUTE |= GPIO_ROUTE_SWOPEN;

#if defined(_EFM32_GECKO_FAMILY) || defined(_EFM32_TINY_FAMILY)
  /* Set location 1 */
  GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC1;
  /* Enable output on pin */
  GPIO->P[2].MODEH &= ~(_GPIO_P_MODEH_MODE15_MASK);
  GPIO->P[2].MODEH |= GPIO_P_MODEH_MODE15_PUSHPULL;
#elif defined(_EFM32_GIANT_FAMILY)
  /* Set location 0 */
  GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC0;
  /* Enable output on pin */
  GPIO->P[5].MODEL &= ~(_GPIO_P_MODEL_MODE2_MASK);
  GPIO->P[5].MODEL |= GPIO_P_MODEL_MODE2_PUSHPULL;
#else
  #error Unknown device family!
#endif

  /* Enable debug clock AUXHFRCO */
  CMU->OSCENCMD = CMU_OSCENCMD_AUXHFRCOEN;

  while(!(CMU->STATUS & CMU_STATUS_AUXHFRCORDY));

  /* Enable trace in core debug */
  CoreDebug->DHCSR |= 1;
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

  /* Enable PC and IRQ sampling output */
  *dwt_ctrl = 0x400113FF;

  /* Set TPIU prescaler to 16 (14 MHz / 16 = 875 kHz SWO speed) */
  *tpiu_prescaler = 0xf;

  /* Set protocol to NRZ */
  *tpiu_protocol = 2;
  *tpiu_ffcr = 0x100;

  /* Unlock ITM and output data */
  ITM->LAR = 0xC5ACCE55;
  ITM->TCR = 0x10009;

  /* ITM Channel 0 is used for UART output */
  ITM->TER |= (1UL << 0);
  
  /* ITM Channel 1 is used for a custom debug output in this example. */
  ITM->TER |= (1UL << 1);  
}

/***************************************************************************//**
* @brief
*   Configure ADC usage for measuring vdd/3 with oversampling.
*******************************************************************************/
static void ADCConfig(void)
{
  ADC_Init_TypeDef       init       = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef singleInit = ADC_INITSINGLE_DEFAULT;

  /* Init common settings for both single conversion and scan mode */
  init.timebase = ADC_TimebaseCalc(0);
  /* Might as well finish conversion as quickly as possibly since polling */
  /* for completion. */
  /* Set ADC clock to 7 MHz, use default HFPERCLK */
  init.prescale = ADC_PrescaleCalc(7000000, 0);

  /* Set oversampling rate */
  init.ovsRateSel = adcOvsRateSel32;

  /* WARMUPMODE must be set to Normal according to ref manual before */
  /* entering EM2. In this example, the warmup time is not a big problem */
  /* due to relatively infrequent polling. Leave at default NORMAL, */

  ADC_Init(ADC0, &init);

  /* Init for single conversion use, measure VDD/3 with 1.25 reference. */
  singleInit.reference = adcRef1V25; //adcRef2V5;
  singleInit.input     = adcSingleInpVDDDiv3; //adcSingleInpCh7;

  /* Enable oversampling rate */
  singleInit.resolution = adcResOVS;

  /* The datasheet specifies a minimum aquisition time when sampling vdd/3 */
  /* 32 cycles should be safe for all ADC clock frequencies */
  singleInit.acqTime = adcAcqTime32;


  ADC_InitSingle(ADC0, &singleInit);
}

/**************************************************************************//**
 * @brief  Main function
 *         The main function demonstrates two ITM trace features;
 *         First printf is utilized to print strings to trace channel 0.
 *         Then a while loop demonstrates how to send data to another 
 *         ITM channel, specifically channel 1.
 *****************************************************************************/
int main(void)
{ 
  SWO_Setup();
  
  BSP_TraceEtmSetup();
  
  printf("Energy Monitor\n");
  
  uint32_t sample;
  uint32_t   voltage;

  /* Enable clocks required */
  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_ADC0, true);

  ADCConfig();

  /* Stay in this loop forever at end of program */
  while (1)
  {
    ADC_Start(ADC0, adcStartSingle);

    /* Wait while conversion is active */
    while (ADC0->STATUS & ADC_STATUS_SINGLEACT) ;

    /* Get ADC result */
    sample = ADC_DataSingleGet(ADC0);

    /* Calculate supply voltage, result is now 16 bit, divide by 65536 */
    voltage = (sample * 1250 * 3) / 65536;

    printf("Count = %d\n", sample);
    printf("Voltage = %d\n", voltage);

    /* wait 100ms in EM2 before next conversion.
     * The rtc interrupt handler is located in the efm32 rtc library; see em_rtc.c */
    RTCDRV_Trigger(100, NULL);
    EMU_EnterEM2(true);
  }
}
